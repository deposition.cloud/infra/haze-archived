import * as pulumi from "@pulumi/pulumi"
import * as k8s from "@pulumi/kubernetes"
import * as kx from "@pulumi/kubernetesx"
import * as cert from "@pulumi/depo-cert-manager"
import * as fs from "fs"

const stack = pulumi.getStack()

const config = new pulumi.Config()
const runnerRegistrationToken = config.requireSecret("runnerRegistrationToken")
const gitlabUrl = 'https://gitlab.com/'

const name = 'pulumi'

const namespace = new k8s.core.v1.Namespace(name, {
  metadata: { name, labels: { name } }
})

const namespaceName = namespace.metadata.name

const appName = 'nginx'
const appLabels = { app: appName }

const deployment = new k8s.apps.v1.Deployment(
  appName, {
  metadata: {
    namespace: namespaceName,
    labels: appLabels,
  },
  spec: {
    selector: { matchLabels: appLabels },
    replicas: 1,
    template: {
      metadata: { labels: appLabels },
      spec: { containers: [{ name: appName, image: 'nginx' }] }
    }
  }
}, {
  parent: namespace
})

// Allocate an IP to the Deployment.
const frontend = new k8s.core.v1.Service(appName, {
  metadata: {
    namespace: namespaceName,
    labels: deployment.spec.template.metadata.labels
  },
  spec: {
    type: "ClusterIP",
    ports: [{ port: 80, targetPort: 80, protocol: "TCP" }],
    selector: appLabels
  }
}, {
  parent: namespace
})

// Ingress Load Balancer Service
const ingressServiceName: string = 'ingress-lb-service'

// Assuming microk8s ingress is running; otherwise enable with `microk8s enable ingress`
const microk8sIngressSelector = {
  name: 'nginx-ingress-microk8s'
}

const ingressName = 'ingress'
const ingressNamespaceName = 'ingress'
const ingressNamespace = new k8s.core.v1.Namespace(ingressName, {
  metadata: { name: ingressNamespaceName, labels: { name: ingressName } }
})

const ingressController = new k8s.helm.v3.Chart("in", {
  repo: "ingress-nginx",
  chart: "ingress-nginx",
  namespace: ingressNamespace.metadata.name
}, {
  parent: ingressNamespace
})

const certName = 'vouch'
const certNamespaceName = 'cert-manager'
const certNamespace = new k8s.core.v1.Namespace(certNamespaceName, {
  metadata: { name: certNamespaceName, labels: { name: certName } }
}, {
  parent: ingressNamespace
})

const certManager = new k8s.helm.v3.Chart(certName, {
  repo: "jetstack",
  chart: "cert-manager",
  namespace: certNamespace.metadata.name, values: { installCRDs: true }
}, {
  parent: certNamespace
})

const clusterIssuerName = 'letsencrypt-staging'

const clusterIssuer = new cert.certmanager.v1.ClusterIssuer(
  clusterIssuerName, {
  metadata: { name: certNamespaceName, labels: { name: certName }, namespace: certNamespace.metadata.name },
  spec: {
    acme: {
      server: "https://acme-staging-v02.api.letsencrypt.org/directory",
      email: "paul@deposition.cloud",
      privateKeySecretRef: {
        name: clusterIssuerName
      },
      solvers: [
        {
          http01: {
            ingress: {
              class: "nginx"
            }
          }
        }
      ]
    }
  }
}, {
  parent: certManager
})

/*
const ingressService = new k8s.core.v1.Service(ingressServiceName, {
  metadata: {
    namespace: 'ingress'
  },
  spec: {
    type: "LoadBalancer",
    ports: [
      { name: 'http', port: 80, targetPort: 80, protocol: "TCP" },
      { name: 'https', port: 443, targetPort: 443, protocol: "TCP" }
    ],
    selector: microk8sIngressSelector
  }
})
*/

const pulumiIngress = new k8s.networking.v1beta1.Ingress('ingress', {
  metadata: {
    namespace: namespaceName,
    labels: deployment.spec.template.metadata.labels,
    annotations: {
      'kubernetes.io/ingress.class': 'nginx',
      // Add the following line (staging first for testing, then apply the prod issuer)
      'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
    }
  },
  spec: {
    tls: [{
      hosts: ['nginx.enfluence.io'],
      secretName: 'default-tls-secret'
    }],
    rules: [{
      host: 'nginx.enfluence.io',
      http: {
        paths: [{
          backend: { serviceName: frontend.metadata.name, servicePort: 80 },
          path: '/',
          pathType: 'ImplementationSpecific'
        }]
      }
    }
    ]
  }
}, {
  parent: frontend
})

/*
* linkerd
*/

const linkerdName = 'mesh'
// const linkerdNamespaceName = 'linkerd'
// const linkerdNamespace = new k8s.core.v1.Namespace(linkerdNamespaceName, {
//   metadata: { name: linkerdNamespaceName, labels: { name: linkerdName } }
// })

let aYearFromNow = new Date()
aYearFromNow.setFullYear(aYearFromNow.getFullYear() + 1)
// console.log(aYearFromNow.toISOString())

const identityTrustAnchorsPEM: string = fs.readFileSync('ca/ca.crt', { encoding: 'utf8', flag: 'r' }).toString().trim()
const crtPEM: string = fs.readFileSync('ca/issuer.crt', { encoding: 'utf8', flag: 'r' }).toString().trim()
const keyPEM: string = fs.readFileSync('ca/issuer.key', { encoding: 'utf8', flag: 'r' }).toString().trim()

// console.log(identityTrustAnchorsPEM)
// console.log(crtPEM)
// console.log(keyPEM)

const linkerd = new k8s.helm.v3.Chart(linkerdName, {
  repo: "linkerd",
  chart: "linkerd2",
  // namespace: linkerdNamespace.metadata.name,
  values: {
    // installNamespace: false,
    // namespace: linkerdNamespace.metadata.name,
    identityTrustAnchorsPEM,
    identity: {
      issuer: {
        tls: { crtPEM, keyPEM },
        crtExpiry: aYearFromNow.toISOString()
      }
    }
  }
})

/*
 * linkerd-viz
 */

const linkerdVizName = 'mesh-viz'
const linkerdVisNamespaceName = 'linkerd-viz'
// const linkerdVizNamespace = new k8s.core.v1.Namespace(linkerdVisNamespaceName, {
//   metadata: { name: linkerdVisNamespaceName, labels: { name: linkerdVizName } }
// }, {
//   parent: linkerdNamespace
// })

const linkerdViz = new k8s.helm.v3.Chart(linkerdVizName, {
  repo: "linkerd",
  chart: "linkerd-viz",
  // namespace: linkerdVizNamespace.metadata.name,
  values: {
    // installNamespace: false,
    // namespace: linkerdVizNamespace.metadata.name,
    // linkerdNamespace: linkerdNamespace.metadata.name
  }
}, {
  // parent: linkerdVizNamespace,
  parent: linkerd,
  // dependsOn: [linkerd]
})

const webIngressAuthSecretName = 'web-ingress-auth'
const basicAuthSecret = new k8s.core.v1.Secret(webIngressAuthSecretName, {
  data: { auth: 'YWRtaW46JGFwcjEkZkouUEY2WVYkemJQNmtHVFBsTzN4dlIuMnlJNjVLLwo=' },
  metadata: { namespace: linkerdVisNamespaceName }
}, {
  parent: linkerdViz
})

const linkerdIngress = new k8s.networking.v1beta1.Ingress('ingress', {
  metadata: {
    // namespace: linkerdNamespace.metadata.name,
    namespace: linkerdVisNamespaceName,
    annotations: {
      'kubernetes.io/ingress.class': 'nginx',
      'nginx.ingress.kubernetes.io/upstream-vhost': '$service_name.$namespace.svc.cluster.local:8084',
      'nginx.ingress.kubernetes.io/configuration-snippet': `
        proxy_set_header Origin "";
        proxy_hide_header l5d-remote-ip;
        proxy_hide_header l5d-server-id;
      `,
      'nginx.ingress.kubernetes.io/auth-type': 'basic',
      'nginx.ingress.kubernetes.io/auth-secret': webIngressAuthSecretName,
      'nginx.ingress.kubernetes.io/auth-realm': 'Authentication Required',
      // Add the following line (staging first for testing, then apply the prod issuer)
      'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
    }
  },
  spec: {
    tls: [{
      hosts: ['linkerd.enfluence.io'],
      secretName: 'default-tls-secret'
    }],
    rules: [{
      host: 'linkerd.enfluence.io',
      http: {
        paths: [{
          backend: { serviceName: 'web', servicePort: 8084 },
          path: '/',
          pathType: 'ImplementationSpecific'
        }]
      }
    }]
  }
}, {
  dependsOn: [linkerdViz]
})


const gitlabRunnerName = 'runner'
const gitlabRunnerNamespaceName = 'gitlab-runner'
const gitlabRunnerNamespace = new k8s.core.v1.Namespace(gitlabRunnerName, {
  metadata: { name: gitlabRunnerNamespaceName, labels: { name: gitlabRunnerName } }
}, {
  parent: ingressController
})

const gitlabRunner = new k8s.helm.v3.Chart("run", {
  repo: "gitlab",
  chart: "gitlab-runner",
  namespace: gitlabRunnerNamespace.metadata.name,
  values: {
    gitlabUrl,
    runnerRegistrationToken,
    checkInterval: 5,
    rbac: { create: true },
    runners: {
      name: `${stack}-runner`,
      tags: 'deposition,docker',
      runUntagged: true,
      locked: false,
      privileged: true
    }
  }
}, {
  parent: gitlabRunnerNamespace
})



/*
const wordpress = new k8s.helm.v3.Chart("wordpress", {
  repo: "bitnami",
  chart: "wordpress",
  version: "10.6.4",
  namespace: namespaceName,
  values: { service: { type: "ClusterIP" } }
})
*/

// When "done", this will print the public IP.
export const ip = frontend.spec.clusterIP
