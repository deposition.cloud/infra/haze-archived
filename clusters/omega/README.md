# Omega Cluster

On a clean Windows 10 [snowflake](https://gitlab.com/deposition.cloud/infra/snowflake).

``` cmd
multipass list
multipass launch --name mk8s --mem 20G --disk 100G --cpus 8

``` cmd
multipass shell mk8s
sudo snap install microk8s --classic --channel=1.20/stable
sudo iptables -P FORWARD ACCEPT
```

In `Hyper-V Manager` add an `External Switch` to make the vm visible to the rest of the network (under `Virtual Switch Manager`), then ensure the `Network Adapter` `Virtual switch:` dropdown for `mk8s` vm is set to `External Switch`.

``` sh
microk8s enable metallb:192.168.1.66-192.168.1.66
microk8s enable dns:192.168.1.1 # assuming a local DNS server already setup
microk8s enable metrics
microk8s enable dashboard
microk8s enable fluentd
microk8s enable helm3
microk8s enable rbac
microk8s enable registry
microk8s enable storage
#microk8s enable ingress # doesn't seem to work, use the helm chart instead
```

``` bash
pulumi config set kubernetes:context omega
```

``` bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
```

Pre-requisites for Helm.

``` bash
helm repo add stable https://charts.helm.sh/stable
helm repo add bitnami https://charts.bitnami.com/bitnami
```

## Cert Manager

``` bash
helm repo add jetstack https://charts.jetstack.io
helm repo update
```

``` bash
cd clusters/omega/cert-manager/
mkdir crds
wget https://github.com/jetstack/cert-manager/releases/download/v1.2.0/cert-manager.crds.yaml --output-document crds/cert-manager.crds.yaml
crd2pulumi crds/cert-manager.crds.yaml --nodejsName depo-cert-manager --nodejsPath cert-manager-types --force
sed -i 's/"version": ""/"version": "0.0.1"/g' cert-manager-types/package.json
npm install typescript --save-dev
cd ../../../
npm install --save clusters/omega/cert-manager/cert-manager-types/
```

## Linkerd

```bash
helm repo add smallstep https://smallstep.github.io/helm-charts/
helm repo update
```

``` bash
helm repo add linkerd https://helm.linkerd.io/stable
helm repo update
```

``` bash
helm repo add gitlab https://charts.gitlab.io
helm repo update
pulumi config set --secret runnerRegistrationToken <value from https://gitlab.com/groups/deposition.cloud/-/settings/ci_cd>
```

Install [GitLab Runner Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html)

### Step Certificates

``` bash
cd ca
step-cli certificate create root.linkerd.cluster.local ca.crt ca.key --profile root-ca --no-password --insecure
step-cli certificate create identity.linkerd.cluster.local issuer.crt issuer.key --profile intermediate-ca --not-after 8760h --no-password --insecure --ca ca.crt --ca-key ca.key
```

## Local Registry

For building and hosting our own docker images, in conjunction with the GitLab-CI Runner integration.

On Windows, update Docker Desktop daemon config `Settings > Docker Engine`, as per [MicroK8s > Using the built-in registry](https://microk8s.io/docs/registry-built-in).

``` json
  "insecure-registries": [
    "registry.lan:32000"
  ],
```

Ensure `registry.lan` is configured as a hostname pointing to the active cluster, for instance, in [Turris > Network > Hostnames](http://192.168.1.1/cgi-bin/luci/admin/network/hosts)

This setup allows us to shift between the `live` and `omega` clusters as needed.

Because the tags won't be what the default MicroK8s setup expects, we'll need to treat the local registry as a 3rd party registry and configure it as such, as per [MicroK8s Working with a private registry](https://microk8s.io/docs/registry-private)

Edit `/var/snap/microk8s/current/args/containerd-template.toml` and add the `registry.lan` mirror (note: this could be the same as the `localhost` mirror).

``` toml
      [plugins."io.containerd.grpc.v1.cri".registry.mirrors."registry.lan:32000"]
        endpoint = ["http://registry.lan:32000"]
```

Then restart MicroK8s

``` bash
microk8s stop
microk8s start
```

### Wrong DNS from host daemon?

Try using your own DNS server, ideally an OpenWRT-based router at the edge of your LAN.

Then, on Windows, update Docker Desktop daemon config `Settings > Docker Engine`.

```json
  "dns": [
    "192.168.1.1"
  ],
```

## Troubleshooting

Might need to add `mk8s.lan` to `/var/snap/microk8s/current/certs/csr.conf.template`

``` bash
$ k get no
Unable to connect to the server: x509: certificate is valid for kubernetes, kubernetes.default, kubernetes.default.svc, kubernetes.default.svc.cluster, kubernetes.default.svc.cluster.local, not mk8s.lan
```

Add a line like `DNS.6 = mk8s.lan`

``` bash
sudo vim /var/snap/microk8s/current/certs/csr.conf.template
```

Wait a few seconds after saving the file for the changes to take effect, then try `kubectl get no` to check connectivity.

### Use as GitLab cluster

Add a line like `DNS.7 = enfluence.io`

Then follow the [setup instructions to add an existing cluster](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster)

Ensure the public facing domain name is listed:

```bash
echo | openssl s_client -showcerts -connect enfluence.io:16443 2>/dev/null | openssl x509 -inform pem -noout -text
```

For that to work, you might need to first configure the firewall rules to point all `16443` traffic to the microk8s cluster.

## References

- [MicroK8s Alternative installs (MacOS/Windows 10/Multipass)](https://microk8s.io/docs/install-alternatives)
- [Linkerd Generating your own mTLS root certificates](https://linkerd.io/2/tasks/generate-certificates/)
